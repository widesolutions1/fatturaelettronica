package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DatiTrasmissionePKs implements Serializable {

    private Integer idFatturaElettronica;
    private Integer idUtente;
    private Integer progressivoInvio;

}
