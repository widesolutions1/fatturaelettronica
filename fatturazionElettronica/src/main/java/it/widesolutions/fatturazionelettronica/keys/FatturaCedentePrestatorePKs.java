package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;
import sun.plugin2.message.Serializer;

import java.io.Serializable;

@Data
public class FatturaCedentePrestatorePKs implements Serializable {

    private Integer idFatturaElettronica;
    private Integer idUtente;

}
