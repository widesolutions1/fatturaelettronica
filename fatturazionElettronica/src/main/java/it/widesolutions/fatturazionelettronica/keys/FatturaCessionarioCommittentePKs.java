package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class FatturaCessionarioCommittentePKs implements Serializable {

    private Integer idFatturaElettronica;
    private Integer idUtente;

}
