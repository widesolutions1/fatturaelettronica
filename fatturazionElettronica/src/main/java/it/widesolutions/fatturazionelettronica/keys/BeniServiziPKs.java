package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class BeniServiziPKs implements Serializable{
//
    private Integer idBeneServizio;
    private Integer idUtente;

}
