package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DettaglioPagamentoPKs implements Serializable {

    private Integer idDettaglioPagamento;
    private Integer idDatiPagamento;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
