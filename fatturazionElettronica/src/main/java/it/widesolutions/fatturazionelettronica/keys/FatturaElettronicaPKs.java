package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;
@Data
public class FatturaElettronicaPKs implements Serializable {

    private Integer idUtente;
    private Integer idFatturaElettronica;

}
