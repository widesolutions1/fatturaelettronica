package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class TerzoIntermediarioPKs implements Serializable {

    private Integer idTerzoIntermediario;
    private Integer idUtente;

}
