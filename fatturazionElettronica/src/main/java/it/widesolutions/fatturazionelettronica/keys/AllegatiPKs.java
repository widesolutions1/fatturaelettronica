package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class AllegatiPKs implements Serializable {

    private Integer idAllegato;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
