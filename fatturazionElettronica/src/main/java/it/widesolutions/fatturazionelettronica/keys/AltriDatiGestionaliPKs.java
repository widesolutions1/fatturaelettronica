package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class AltriDatiGestionaliPKs implements Serializable {

    private Integer idAltriDatiGestionali;
    private Integer idDettaglioLinee;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
