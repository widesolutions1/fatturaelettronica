package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class ContiCorrentiPKs implements Serializable {

    private Integer idContoCorrente;
    private Integer idUtente;

}
