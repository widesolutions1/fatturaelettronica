package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImportFatturePKs implements Serializable {

    private Integer idImportFattura;
    private Integer idUtente;
    private Integer idFatturaElettronica;
    private Integer idImportSession;
    private Integer idImport;

}
