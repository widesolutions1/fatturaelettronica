package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class ImportPKs implements Serializable {

    private Integer idUtente;
    private Integer idImportSession;
    private Integer idImport;

}
