package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DatiContrattoPKs implements Serializable {

    private Integer idDatiContratto;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
