package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class MesiPKs implements Serializable {

    private Integer mese;
    private Integer anno;

}
