package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class TicketMessaggioPKs implements Serializable {

    private Integer idUtente;
    private Integer idTicket;
    private Integer idTicketMessaggio;

}
