package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class RappresentanteFiscalePKs implements Serializable {

    private Integer idRappresentanteFiscale;
    private Integer idUtente;

}
