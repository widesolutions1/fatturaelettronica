package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProvincePKs implements Serializable {

    private Integer idRegione;
    private Integer idProvincia;

}
