package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DatiPagamentoPKs implements Serializable {

    private Integer idDatiPagamento;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
