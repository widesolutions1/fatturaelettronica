package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DettaglioLineePKs implements Serializable {

    private Integer idDettaglioLinee;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
