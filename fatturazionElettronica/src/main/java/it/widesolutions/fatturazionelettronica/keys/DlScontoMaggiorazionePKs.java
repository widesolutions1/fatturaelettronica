package it.widesolutions.fatturazionelettronica.keys;

import lombok.Data;

import java.io.Serializable;

@Data
public class DlScontoMaggiorazionePKs implements Serializable {

    private Integer idScontoMaggiorazione;
    private Integer idDettaglioLinee;
    private Integer idFatturaElettronica;
    private Integer idUtente;

}
