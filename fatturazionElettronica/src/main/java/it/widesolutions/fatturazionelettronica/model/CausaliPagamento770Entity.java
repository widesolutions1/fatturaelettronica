package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="CAUSALIPAGAMENTO770")
public class CausaliPagamento770Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCAUSALEPAGAMENTO770")
    private String idCausalePagamento770;

    @Column(name="CAUSALEPAGAMENTO770")
    private String causalePagamento770;

}
