package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DatiContrattoPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="DATICONTRATTO")
@IdClass(DatiContrattoPKs.class)
public class DatiContrattoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDATICONTRATTO")
    private Integer idDatiContratto;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="RIFERIMENTONUMEROLINEA")
    private Integer riferimentoNumeroLinea;

    @Column(name="IDDOCUMENTO")
    private Integer idDocumento;

    @Column(name="DATA")
    private Date data;

    @Column(name="NUMITEM")
    private String numItem;

    @Column(name="CODICECOMMESSACONVENZIONE")
    private String codiceCommessaConvenzione;

    @Column(name="CODICECUP")
    private String codiceCup;

    @Column(name="CODICECIG")
    private String codiceCig;

    @Column(name="DATAINSERIMENTO")
    private String dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private String dataAggiornamento;

    @OneToMany(mappedBy = "datiContrattoToDcNumeroLinea")
    List<DcNumeroLineaEntity> dcNumeroLinee;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})

    public FatturaElettronicaEntity fatturaElettronicaToDatiContratto;


}
