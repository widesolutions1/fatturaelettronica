package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="PROFILI")
public class ProfiliEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDPROFILO")
    private String idProfilo;

    @Column(name="PROFILO")
    private String profilo;

    @Column(name="MESI_SERVIZIO")
    private Integer mesiServizio;

}
