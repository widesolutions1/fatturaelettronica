package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DettaglioPagamentoPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="DETTAGLIOPAGAMENTO")
@IdClass(DettaglioPagamentoPKs.class)
public class DettaglioPagamentoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDETTAGLIOPAGAMENTO")
    private Integer idDettaglioPagamento;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDATIPAGAMENTO")
    private Integer idDatiPagamento;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="BENEFICIARIO")
    private String beneficiario;

    @Column(name="MODALITAPAGAMENTO")
    private String modalitaPagamento;

    @Column(name="DATARIFERIMENTOTERMINIPAGAMENTO")
    private Date dataRiferimentoTerminiPagamento;

    @Column(name="GIORNITERMINIPAGAMENTO")
    private Integer giorniTerminiPagamento;

    @Column(name="DATASCADENZAPAGAMENTO")
    private Date dataScadenzaPagamento;

    @Column(name="IMPORTOPAGAMENTO")
    private Integer importoPagamento;

    @Column(name="CODUFFICIOPOSTALE")
    private String codUfficioPostale;

    @Column(name="COGNOMEQUIETANZATE")
    private String cognomeQuietanzante;

    @Column(name="NOMEQUIETANZANTE")
    private String nomeQuietanzante;

    @Column(name="CFQUIETANZANTE")
    private String cfQuietanzante;

    @Column(name="TITOLOQUIETANZANTE")
    private String titoloQuietanzante;

    @Column(name="ISTITUTOFINANZIARIO")
    private String istitutoFinanziario;

    @Column(name="IBAN")
    private String iban;

    @Column(name="ABI")
    private String abi;

    @Column(name="CAB")
    private String cab;

    @Column(name="BIC")
    private String bic;

    @Column(name="SCONTOPAGAMENTOANTICIPATO")
    private Integer scontoPagamentoAnticipato;

    @Column(name="DATALIMITEPAGAMENTOANTICIPATO")
    private Date dataLimitePagamentoAnticipato;

    @Column(name="PENALITAPAGAMENTIRITARDATI")
    private Integer penalitaPagamentiRitardati;

    @Column(name="DATADECORRENZAPENALE")
    private Date dataDecorrenzaPenale;

    @Column(name="CODICEPAGAMENTO")
    private String codicePagamento;






}
