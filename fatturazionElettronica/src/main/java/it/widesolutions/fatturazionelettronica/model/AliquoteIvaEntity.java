package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;


@Entity
@Data
@Table(name="ALIQUOTEIVA")
public class AliquoteIvaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDALIQUOTAIVA")
    private Integer idAliquotaIva;

    @Column(name="ALIQUOTAIVA")
    private String aliquotaIva;


}
