package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TIPIPAGAMENTI")
public class TipiPagamentiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTIPOPAGAMENTO")
    private String idTipoPagamento;

    @Column(name="TIPOPAGAMENTO")
    private String tipoPagamento;

}
