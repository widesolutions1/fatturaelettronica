package it.widesolutions.fatturazionelettronica.model;
import lombok.*;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Data
@Table(name = "UTENTI")
public class UtentiEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDUTENTE")
    private Integer IdUtente;

    @Column(name = "USERNAME")
    private String UserName;

    @Column(name ="PASSWORD")
    private String Password;

    @Column(name ="IDPROFILO")
    private Integer IdProfilo;

    @Column(name = "ATTIVO")
    private Boolean Attivo ;

    @Column(name = "EMAIL")
    private String Email ;

    @Column(name = "PASSWORD_RESET_HASH")
    private String PasswordResetHash;

    @Column(name = "DATA_SCADENZA_SERVIZIO")
    private Date DataScadenzaServizio;

    @Column(name = "FORZA_RESET_PASSWORD")
    private Boolean ForzaResetPassword;

    @Column(name = "WPORDER")
    private String WPorder;

    @Column(name = "WPITEMNUMBER")
    private String WPitemNumber;

    @Column(name = "NOME")
    private String Nome;

    @Column(name = "COGNOME")
    private String Cognome;

    @Column(name = "DATA_INSERIMENTO")
    private Date DataInserimento;

    @Column(name = "EMAIL_COMMERCIALISTA")
    private String EmailCommercialista;

    @Column(name = "FREQUENZA_INVIO_EMAIL")
    private String FrequenzaIvioEmail;

    @Column(name = "ORARIO_INVIO_EMAIL")
    private String OrarioInvioEmail;

    @Column(name = "INVIO_EMAIL_SETTIMANALE")
    private String InvioEmailSettimanale;

    @Column(name = "INVIO_EMAIL_MENSILE")
    private String InvioEmailMensile;

    @Column(name = "INVIO_EMAIL_ATTIVO")
    private Boolean InvioEmailAttivo;

    @Column(name = "AGGIORNAMENTO_FORNITORI")
    private Boolean AggiornamentoFornitori;

    @Column(name = "EMAIL_FATTURE_RICEVUTE")
    private Boolean EmailFattureRicevute ;

    @Column(name = "ULTIMO_INVIO_COMMERCIALISTA")
    private Date UltimoInvioCommercialista;

    @OneToMany(mappedBy = "utenteToBeni")
    List<BeniServiziEntity>beniServizi;

    @OneToMany(mappedBy = "utenteToContiCorrenti")
    List<ContiCorrentiEntity>contiCorrenti;



}
