package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TIPIDOCUMENTI")
public class TipiDocumentiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTIPODOCUMENTO")
    private String idTipoDocumento;

    @Column(name="TIPODOCUMENTO")
    private String tipoDocumento;

    @Column(name="SELEZIONABILE")
    private String selezionabile;

}
