package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TICKET_STATO")
public class TicketStatoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTICKET_STATO")
    private Integer idTicketStato;

    @Column(name="STATO_TICKET")
    private String statoTicket;

}
