package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="CONDIZIONIPAGAMENTO")
public class CondizioniPagamentoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCONDIZIONE_PAGAMENTO")
    private String idCondizionePagamento;

    @Column(name="CONDIZIONE_PAGAMENTO")
    private String condizionePagamento;

    @Column(name="GIORNI_DILAZIONE")
    private Integer giorniDilazione;

    @Column(name="MESI_DILAZIONE")
    private String mesiDilazione;

    @Column(name="FINE_MESE")
    private String fineMese;

}
