package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.FatturaCedentePrestatorePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="FATTURA_CEDENTEPRESTATORE")
@IdClass(FatturaCedentePrestatorePKs.class)
public class FatturaCedentePrestatoreEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA", unique = true)
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="IDCODICE")
    private String idCodice;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="DENOMINAZIONE")
    private String denominazione;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="TITOLO")
    private String titolo;

    @Column(name="CODEORI")
    private String codeOri;

    @Column(name="ALBOPROFESSIONALE")
    private String alboProfessionale;

    @Column(name="PROVINCIAALBO")
    private String provinciaAlbo;

    @Column(name="NUMEROISCRIZIONEALBO")
    private String numeroIscrizioneAlbo;

    @Column(name="DATAISCRIZIONEALBO")
    private Date dataIscrizioneAlbo;

    @Column(name="REGIMEFISCALE")
    private String regimeFiscale;

    @Column(name="RIFERIMENTOAMMINISTRAZIONE")
    private String riferimentoAmministrazione;

    @Column(name="IDPAESE")
    private String idPaese;

    @Column(name="SE_INDIRIZZO")
    private String seIndirizzo;

    @Column(name="SE_NUMEROCIVICO")
    private String seNumeroCivico;

    @Column(name="SE_CAP")
    private String seCap;

    @Column(name="SE_COMUNE")
    private String seComune;

    @Column(name="SE_PROVINCIA")
    private String seProvincia;

    @Column(name="SE_NAZIONE")
    private String seNazione;

    @Column(name="SO_INDIRIZZO")
    private String soIndirizzo;

    @Column(name="SO_NUMEROCIVICO")
    private String soNumeroCivico;

    @Column(name="SO_CAP")
    private String soCap;

    @Column(name="SO_COMUNE")
    private String soComune;

    @Column(name="SO_PROVINCIA")
    private String soProvincia;

    @Column(name="SO_NAZIONE")
    private String soNazione;

    @Column(name="UFFICIO")
    private String ufficio;

    @Column(name="NUMEROREA")
    private String numeroRea;

    @Column(name="CAPITALESOCIALE")
    private Integer capitaleSociale;

    @Column(name="SOCIOUNICO")
    private String socioUnico;

    @Column(name="STATOLIQUIDAZIONE")
    private String statoLiquidazione;

    @Column(name="TELEFONO")
    private String telefono;

    @Column(name="FAX")
    private String fax;

    @Column(name="EMAIL")
    private String email;

    @Column(name="DATA_INSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATA_AGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="PROGRESSIVOINVIO")
    private Integer progressivoInvio;

}
