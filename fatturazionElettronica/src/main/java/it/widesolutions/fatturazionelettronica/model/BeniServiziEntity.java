package it.widesolutions.fatturazionelettronica.model;
import it.widesolutions.fatturazionelettronica.keys.BeniServiziPKs;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="BENISERVIZI")
@IdClass(BeniServiziPKs.class)
public class BeniServiziEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDBENESERVIZIO")
    private Integer idBeneServizio;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE", unique = true)
    private Integer idUtente;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="UNITAMISURA")
    private String unitaMisura;

    @Column(name="PREZZOUNITARIO")
    private Integer prezzoUnitario;

    @Column(name="NATURA")
    private String natura;

    @Column(name="ALIQUOTAIVA")
    private Integer aliquotaIva;

    @Column(name="RITENUTA")
    private String ritenuta;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="CODICE", unique = true)
    private String codice;


    @ManyToOne
    @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)
    public UtentiEntity utenteToBeni;

}
