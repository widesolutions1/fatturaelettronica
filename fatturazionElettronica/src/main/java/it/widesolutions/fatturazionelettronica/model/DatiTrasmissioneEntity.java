package it.widesolutions.fatturazionelettronica.model;


import it.widesolutions.fatturazionelettronica.keys.DatiTrasmissionePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="DATITRASMISSIONE")
@IdClass(DatiTrasmissionePKs.class)
public class DatiTrasmissioneEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="PROGRESSIVOINVIO")
    private Integer progressivoInvio;

    @Column(name="FORMATOTRASMISSIONE")
    private String formatoTrasmissione;

    @Column(name="CODICEDESTINATARIO")
    private String codiceDestinatario;

    @Column(name="PECDESTINATARIO")
    private String pecDestinatario;

    @Column(name="IDPAESE")
    private String idPaese;

    @Column(name="IDCODICE")
    private String idCodice;

    @Column(name="CT_TELEFONO")
    private String ctTelefono;

    @Column(name="CT_EMAIL")
    private String ctEmail;

    @Column(name="NOMEFILE")
    private String nomeFile;

    @Column(name="IDENTIFICATIVORELAY")
    private Integer identificativoRelay;

    @Column(name="DATAORARICEZIONERELAY")
    private Date dataOraRicezioneRelay;

    @Column(name="ERRMSG")
    private String errMsg;

    @Column(name="XMLFATTURA", columnDefinition = "TEXT") //clob! in prova
    private String xmlFattura;

    @Column(name="CSFILE")
    private String csFile;

    @Column(name="AGGDOC")
    private Date aggDoc;

    @Column(name="MIMETYPE")
    private String mimeType;

    @Column(name="DATAINSERIMENTO")
    private Date dataIniserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="XMLFATTURABLOB", columnDefinition = "TEXT") //blob! in prova
    private String xmlFatturaBlob;

    @Column(name="IDENTIFICATIVOSDI")
    private Integer identificativoSdi;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="IDCEDENTE_PRESTATORE")
    private Integer idCedentePrestatore;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})

    public FatturaElettronicaEntity fatturaElettronicaToDatiTrasmissione;

}
