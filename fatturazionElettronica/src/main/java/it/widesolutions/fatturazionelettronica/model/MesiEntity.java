package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.MesiPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="MESI")
@IdClass(MesiPKs.class)
public class MesiEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="MESE")
    private Integer mese;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ANNO")
    private Integer anno;

    @Column(name="GIORNI_LAVORATIVI")
    private Integer giorniLavorativi;

    @Column(name="IDMOVIMENTO_CASHFLOW_IVA")
    private Integer idMovimentoCashflowIva;


}
