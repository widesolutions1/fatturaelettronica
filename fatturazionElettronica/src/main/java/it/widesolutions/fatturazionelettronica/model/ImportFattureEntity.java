package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.ImportFatturePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="IMPORTFATTURE")
@IdClass(ImportFatturePKs.class)
public class ImportFattureEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDIMPORTFATTURA")
    private Integer idImportFattura;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDIMPORTSESSION")
    private Integer idImportSession;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDIMPORT")
    private Integer idImport;

    @Column(name="TIPODOCUMENTO")
    private Integer tipoDocumento;

    @Column(name="DATAINSERIMENTO")
    private Integer dataInserimente;

    @Column(name="DATAAGGIORNAMENTO")
    private Integer dataAggiornamento;

    @Column(name="RC_PREELABORAZIONE")
    private Integer rcPreElaborazione;

    @Column(name="ERMMSG_PREELABORAZIONE")
    private Integer ermMsgPreElaborazione;

    @Column(name="DGD_DATA")
    private Integer dgdData;

    @Column(name="DGD_NUMERO")
    private Integer dgdNumero;

    @Column(name="DGD_IMPORTOTOTALEDOCUMENTO")
    private Integer dgdImportoTotaleDocumento;

}
