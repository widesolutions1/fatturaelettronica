package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="ANNI")
public class AnniEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ANNO")
    private Integer anno;

    @Column(name="DESCRIZIONE")
    private String descrizione;

}
