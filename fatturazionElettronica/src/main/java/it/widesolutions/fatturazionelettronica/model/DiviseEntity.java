package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="DIVISE")
public class DiviseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ALPHA3_CODE")
    private String alpha3Code;

    @Column(name="DIVISA")
    private String divisa;

}
