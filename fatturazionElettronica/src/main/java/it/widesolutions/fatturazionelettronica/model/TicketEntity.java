package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.TicketPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="TICKET")
@IdClass(TicketPKs.class)
public class TicketEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTICKET")
    private Integer idTicket;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTICKET_STATO")
    private Integer idTicketStato;

    @Column(name="DESCRIZIONE", columnDefinition = "TEXT")
    private String descrizione;

    @Column(name="TITOLO")
    private String titolo;

    @Column(name="ALLEGATO", columnDefinition = "TEXT")
    private String allegato;

    @Column(name="MIMETYPE")
    private String mimeType;

    @Column(name="AGGDOC")
    private Date aggDoc;

    @Column(name="NOMEFILE")
    private String nomeFile;

    @Column(name="CSFILE")
    private String csFile;

    @Column(name="DATA_INSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATA_AGGIORNAMENTO")
    private Date dataAggiornamento;

}
