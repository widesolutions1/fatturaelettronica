package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.ProvincePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="PROVINCE")
@IdClass(ProvincePKs.class)
public class ProvinceEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDREGIONE")
    private Integer idRegione;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDPROVINCIA", unique = true)
    private Integer idProvincia;

    @Column(name="PROVINCIA")
    private String provincia;

    @Column(name="CODICE_ISTAT_PROVINCIA")
    private String codiceIstatProvincia;

    @Column(name="SIGLA_AUTOMOBILISTICA", unique = true)
    private String siglaAutomobilistica;

}
