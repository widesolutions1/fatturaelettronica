package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TIPICOMMITTENTI")
public class TipiCommittentiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTIPOCOMMITTENTE")
    private String idTipoCommittente;

    @Column(name="TIPOCOMMITTENTE")
    private String tipoCommittente;

}
