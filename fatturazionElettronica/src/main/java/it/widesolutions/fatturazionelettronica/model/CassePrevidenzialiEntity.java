package it.widesolutions.fatturazionelettronica.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Table(name="CASSEPREVIDENZIALI")
public class CassePrevidenzialiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCASSAPREVIDENZIALE")
    private Integer idCassaPrevidenziale;

    @Column(name="CASSAPREVIDENZIALE")
    private String cassaPrevidenziale;

}
