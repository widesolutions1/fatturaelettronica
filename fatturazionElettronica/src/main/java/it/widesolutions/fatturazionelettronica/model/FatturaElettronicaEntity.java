package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.FatturaElettronicaPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "FATTURAELETTRONICA")
@IdClass(FatturaElettronicaPKs.class)
public class FatturaElettronicaEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDUTENTE", unique = true)
    private Integer idUtente;

    @Id
    @Column(name = "IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Column(name = "VERSIONE")
    private String versione ;

    @Column(name = "DG_SOGGETTOEMITTENTE")
    private String dgSoggettoMittente;

    @Column(name = "DGD_TIPODOCUMENTO")
    private String dgdTipoDocumento ;

    @Column(name = "DGD_DIVISA")
    private String dgdDivisa ;

    @Column(name = "DGD_DATA")
    private String dgdData ;

    @Column(name = "DGD_NUMERO", unique = true)
    private String dgdNumero ;

    @Column(name = "DGD_IMPORTOTOTALEDOCUMENTO")
    private Integer dgdImportoTotaleDocumento;

    @Column(name = "DGD_ARROTONDAMENTO")
    private Integer dgdArrotondamento;

    @Column(name = "DGD_CAUSALE")
    private String DgdCausale ;

    @Column(name = "DGD_ART73")
    private String dgdArt73 ;

    @Column(name = "DR_TIPORITENUTA")
    private String drTiporitenuta;

    @Column(name = "DR_IMPORTORITENUTA")
    private Integer drImportoRitenuta;

    @Column(name = "DR_ALIQUOTARITENUTA")
    private Integer drAliquotaRitenuta;

    @Column(name = "DR_CAUSALEPAGAMENTO")
    private String drCausalePagamento;

    @Column(name = "DB_BOLLOVIRTUALE")
    private String dbBolloVirtuale;

    @Column(name = "DB_IMPORTOBOLLO")
    private Integer dbImportoBollo;

    @Column(name = "DATAINSERIMENTO")
    private String dataInserimento;

    @Column(name = "DATAAGGIORNAMENTO")
    private String dataAggiornamento;

    @Column(name = "IDCEDENTE_PRESTATORE")
    private Integer ideCedentePrestatore;

    @Column(name = "IDCESSIONARIOCOMMITTENTE")
    private Integer idCessionarioCommittente;

    @Column(name = "MODALITAINVIO")
    private String modalitaInvio;

    @Column(name = "CODICEDESTINATARIO")
    private String codiceDestinatario;

    @Column(name = "PECDESTINATARIO")
    private String pecdestinatario;

    @Column(name = "IDSTATOFATTURA_PREV")
    private String idStatoFatturaPrev;

    @Column(name = "IDSTATOFATTURA_NEXT")
    private String idStatoFatturaNext;

    @Column(name = "PROGRESSIVORICEZIONE")
    private Integer progressivoRicezione;

    @Column(name = "IDCEDENTE_PRESTATORE_FACQUISTO")
    private Integer idCedentePrestatoreFacquisto;

    @Column(name = "IDRAPPRESENTANTEFISCALE")
    private Integer idRappresentateFiscale;

    @Column(name = "IDTERZOINTERMEDIARIO")
    private Integer idTerzoIntermediario;

    @Column(name = "DATA_CONSEGNA")
    private String dataConsegna;

    @Column(name = "DIREZIONEDOCUMENTO")
    private String direzioneDocumento;

    @Column(name = "CC_IDCODICE")
    private String ccIdCodice;

    @Column(name = "CC_IDPAESE")
    private String ccIdPaese;

    @Column(name = "CP_IDCODICE")
    private String cpIdCodice;

    @Column(name = "CP_IDPAESE")
    private String cpIdPaese;

    @Column(name = "ERRORE_SDI",columnDefinition = "TEXT")
    private String erroreSdi ;

    @Column(name = "PROGRESSIVOINVIO")
    private Integer progressivoInvio;

    @Column(name = "DT_MEZZOTRASPORTO")
    private String dtMezzoTrasporto;

    @Column(name = "DT_CAUSALETRASPORTO")
    private String dtCausaleTrasporto;

    @Column(name = "DT_NUMEROCOLLI")
    private Integer dtNumeroColli;

    @Column(name = "DT_DESCRIZIONE")
    private String dtDescrizione;

    @Column(name = "DT_UNITAMISURAPESO")
    private String dtUnitaMisuraPeso;

    @Column(name = "DT_PESOLORDO")
    private Integer dtPesoLordo;

    @Column(name = "DT_DATAORARITIRO")
    private String dtDataOraRitiro;

    @Column(name = "DT_DATAINIZIOTRASPORTO")
    private String dtDataInizioTrasporto;

    @Column(name = "DT_TIPORESA")
    private String dtTipoResa;

    @Column(name = "DT_IR_INDIRIZZO")
    private String dtIrIndirizzo;

    @Column(name = "DT_IR_NUMEROCIVICO")
    private String dtIrNumeroCivico;

    @Column(name = "DT_IR_COMUNE")
    private String dtIrComune;

    @Column(name = "DT_IR_PROVINCIA")
    private String dtIrProvincia;

    @Column(name = "DT_IR_NAZIONE")
    private String dtIrNaione;

    @Column(name = "DT_DATAORACONSEGNA")
    private String dtDataOraConsegna;

    @Column(name = "DT_IR_CAP")
    private String dtIrCap;

    @Column(name = "DT_PESONETTO")
    private Integer dtPesoNetto;

    @OneToMany(mappedBy = "fatturaElettronicaToAllegati")
    List<AllegatiEntity> allegati;

    @OneToMany(mappedBy = "fatturaElettronicaToDatiContratto")
    List<DatiContrattoEntity> datiContratti;

    @OneToMany(mappedBy = "fatturaElettronicaToDatiPagamento")
    List<DatiPagamentoEntity> datiPagamenti;

    @OneToMany(mappedBy = "fatturaElettronicaToDatiTrasmissione")
    List<DatiTrasmissioneEntity> datiTrasmissione;

}
