package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TIPIRITENUTE")
public class TipiRitenuteEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTIPORITENUTA")
    private String idTipoRitenuta;

    @Column(name="TIPORITENUTA")
    private String tipoRitenuta;

    @Column(name="SELEZIONABILE")
    private String selezionabile;

}
