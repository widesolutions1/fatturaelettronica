package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="FATTURAELETTRONICA_STATO")
public class FatturaElettronicaStatoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDSTATOFATTURA")
    private String idStatoFattura;

    @Column(name="STATOFATTURA")
    private String statoFattura;

}
