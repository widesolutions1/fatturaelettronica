package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="COMMERCIALISTI")
public class Commercialisti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCOMMERCIALISTA")
    private Integer idCommercialista;

    @Column(name="USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    @Column(name="ATTIVO")
    private String attivo;

    @Column(name="EMAIL")
    private String email;

    @Column(name="PASSWORD_RESET_HASH")
    private String passwordResetHash;

    @Column(name="DATA_SCADENZA_SERVIZIO")
    private Date dataScadenzaServizio;

    @Column(name="FORZA_RESET_PASSWORD")
    private String forzaResetPassword;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="PARTITAIVA")
    private String partitaIva;

    @Column(name="SE_INDIRIZZO")
    private String seIndirizzo;

    @Column(name="SE_NUMEROCIVICO")
    private String seNumeroCivico;

    @Column(name="SE_CAP")
    private String seCap;

    @Column(name="SE_COMUNE")
    private String seComune;

    @Column(name="SE_PROVINCIA")
    private String seProvincia;

    @Column(name="SE_NAZIONE")
    private String seNazione;

    @Column(name="DATA_INSERIMENTO")
    private Date dataInserimento;

}
