package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.AltriDatiGestionaliPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="ALTRIDATIGESTIONALI")
@IdClass(AltriDatiGestionaliPKs.class)
public class AltriDatiGestionaliEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDALTRIDATIGESTIONALI")
    private Integer idAltriDatiGestionali;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDETTAGLIOLINEE")
    private Integer idDettaglioLinee;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="TIPODATO")
    private String tipoDato;

    @Column(name="RIFERIMENTOTESTO")
    private String riferimentoTesto;

    @Column(name="RIFERIMENTONUMERO")
    private Integer riferimentoNumero;

    @Column(name="RIFERIMENTODATA")
    private Date riferimentoData;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDDETTAGLIOLINEE", insertable = false, updatable = false),
            @JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})

    public DatiContrattoEntity dettaglioLineeToAltriDatiGestionali;

}
