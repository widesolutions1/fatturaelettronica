package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DettaglioLineePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name="DETTAGLIOLINEE")
@IdClass(DettaglioLineePKs.class)
public class DettaglioLineeEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDETTAGLIOLINEE")
    private Integer idDettaglioLinee;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="TIPOCESSIONEPRESTAZIONE")
    private String tipoCessionePrestazione;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="QUANTITA")
    private Integer quantita;

    @Column(name="UNITAMISURA")
    private String unitaMisura;

    @Column(name="DATAINIZIOPERIODO")
    private Date dataInizioPeriodo;

    @Column(name="DATAFINEPERIODO")
    private Date dataFinePeriodo;

    @Column(name="PREZZOUNITARIO")
    private Integer prezzoUnitario;

    @Column(name="PREZZOTOTALE")
    private Integer prezzoTotale;

    @Column(name="ALIQUOTAIVA")
    private Integer aliquotaIva;

    @Column(name="RITENUTA")
    private String ritenuta;

    @Column(name="NATURA")
    private String natura;

    @Column(name="RIFERIMENTOAMMINISTRAZIONE")
    private String riferimentoAmministrazione;

    @Column(name="NUMEROLINEA")
    private Integer numeroLinea;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="IMPORTOSCONTOMAGGIORAZIONE")
    private Integer importoScontoMaggiorazione;

    @Column(name="PREZZOFINALE_TBD")
    private Integer prezzoFinaleTbd;

    @Column(name="SCONTIMAGGIORAZIONE")
    private String scontiMaggiorazione;

    @Column(name="IMPOSTA")
    private Integer imposta;

    @Column(name="ESIGIBILITAIVA")
    private String esigibilitaIva;

    @OneToMany(mappedBy = "datiContrattoToDcNumeroLinea")
    List<DcNumeroLineaEntity> dettaglioLineeToAltriDatiGestionali;


}
