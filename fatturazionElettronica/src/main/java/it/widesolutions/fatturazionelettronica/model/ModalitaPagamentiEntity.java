package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="MODALITAPAGAMENTI")
public class ModalitaPagamentiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDMODALITAPAGAMENTO")
    private String idModalitaPagamento;

    @Column(name="MODALITAPAGAMENTO")
    private String modalitaPagamento;

}
