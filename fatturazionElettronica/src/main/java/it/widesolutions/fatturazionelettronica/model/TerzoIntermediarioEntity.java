package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.TerzoIntermediarioPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="TERZOINTERMEDIARIO")
@IdClass(TerzoIntermediarioPKs.class)
public class TerzoIntermediarioEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTERZOINTERMEDIARIO")
    private Integer idTerzoIntermediario;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="IDPAESE")
    private String idPaese;

    @Column(name="IDCODICE")
    private String idCodice;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="DENOMINAZIONE")
    private String denominazione;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="TITOLO")
    private String titolo;

    @Column(name="CODEORI")
    private String codeOri;

    @Column(name="DATA_INSERIMENTO")
    private String dataInserimento;

    @Column(name="DATA_AGGIORNAMENTO")
    private String dataAggiornamento;

}
