package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DcNumeroLineaPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="DC_NUMEROLINEA")
@IdClass(DcNumeroLineaPKs.class)
public class DcNumeroLineaEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDATICONTRATTO")
    private Integer idDatiContratto;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="RIFERIMENTONUMEROLINEA")
    private Integer riferimentoNumeroLinea;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDDATICONTRATTO", insertable = false, updatable = false),
            @JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})

    public DatiContrattoEntity datiContrattoToDcNumeroLinea;

}
