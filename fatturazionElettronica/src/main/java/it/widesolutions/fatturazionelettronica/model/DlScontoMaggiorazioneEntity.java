package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DlScontoMaggiorazionePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name="DL_SCONTOMAGGIORAZIONE")
@IdClass(DlScontoMaggiorazionePKs.class)
public class DlScontoMaggiorazioneEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDSCONTOMAGGIORAZIONE")
    private Integer idScontoMaggiorazione;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDETTAGLIOLINEE")
    private Integer idDettaglioLinee;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="TIPO")
    private String tipo;

    @Column(name="PERCENTUALE")
    private Integer percentuale;

    @Column(name="IMPORTO")
    private Integer importo;

}
