package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.FatturaCessionarioCommittentePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="FATTURA_CESSIONARIOCOMMITTENTE")
@IdClass(FatturaCessionarioCommittentePKs.class)
public class FatturaCessionarioCommittenteEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA", unique = true)
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="IDPAESE")
    private String idPaese;

    @Column(name="IDCODICE")
    private String idCodice;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="DENOMINAZIONE")
    private String denominazione;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="TITOLO")
    private String titolo;

    @Column(name="CODEORI")
    private String codeOri;

    @Column(name="RF_DENOMINAZIONE")
    private String rfDenominazione;

    @Column(name="RF_NOME")
    private String rfNome;

    @Column(name="RF_COGNOME")
    private String rfCognome;

    @Column(name="RF_IDPAESE")
    private String rfIdPaese;

    @Column(name="RF_IDCODICE")
    private String rfIdCodice;

    @Column(name="SE_INDIRIZZO")
    private String seIndirizzo;

    @Column(name="SE_NUMEROCIVICO")
    private String seNumeroCivico;

    @Column(name="SE_CAP")
    private String seCap;

    @Column(name="SE_COMUNE")
    private String seComune;

    @Column(name="SE_PROVINCIA")
    private String seProvincia;

    @Column(name="SE_NAZIONE")
    private String seNazione;

    @Column(name="SO_INDIRIZZO")
    private String soIndirizzo;

    @Column(name="SO_NUMEROCIVICO")
    private String soNumeroCivico;

    @Column(name="SO_CAP")
    private String soCap;

    @Column(name="SO_COMUNE")
    private String soComune;

    @Column(name="SO_PROVINCIA")
    private String soProvincia;

    @Column(name="SO_NAZIONE")
    private String soNazione;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="MODALITAINVIO")
    private String modalitaInvio;

    @Column(name="CODICEDESTINATARIO")
    private String codiceDestinatario;

    @Column(name="PECDESTINATARIO")
    private String pecDestinatario;

}
