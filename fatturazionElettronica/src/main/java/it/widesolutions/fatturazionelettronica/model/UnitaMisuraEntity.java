package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="UNITAMISURA")
public class UnitaMisuraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUNITAMISURA")
    private String idUnitaMisura;

    @Column(name="UNITAMISURA")
    private String unitaMisura;

}
