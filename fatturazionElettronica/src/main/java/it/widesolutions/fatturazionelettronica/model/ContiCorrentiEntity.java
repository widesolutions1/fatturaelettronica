package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.ContiCorrentiPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="CONTICORRENTI")
@IdClass(ContiCorrentiPKs.class)
public class ContiCorrentiEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDCONTOCORRENTE")
    private Integer idContoCorrente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="ISTITUTOFINANZIARIO")
    private String istitutoFinanziario;

    @Column(name="IBAN")
    private String iban;

    @Column(name="ABI")
    private String abi;

    @Column(name="CAB")
    private String cab;

    @Column(name="BIC")
    private String bic;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="NUMERO_CONTO")
    private String numeroConto;

    @Column(name="NOMECONTO")
    private String nomeconto;

    @ManyToOne
    @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)
    public UtentiEntity utenteToContiCorrenti;

}
