package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="TIPICESSIONE")
public class TipiCessioneEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTIPOCESSIONE")
    private String idTipoCessione;

    @Column(name="TIPOCESSIONE")
    private String tipoCessione;

}
