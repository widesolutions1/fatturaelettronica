package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.RappresentanteFiscalePKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="RAPPRESENTANTEFISCALE")
@IdClass(RappresentanteFiscalePKs.class)
public class RappresentanteFiscaleEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDRAPPRESENTANTEFISCALE")
    private Integer idRappresentanteFiscale;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="IDPAESE")
    private String idPaese;

    @Column(name="IDCODICE")
    private String idCodice;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="DENOMINAZIONE")
    private String denominazione;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="TITOLO")
    private String titolo;

    @Column(name="CODEORI")
    private String codeOri;

    @Column(name="DATA_INSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATA_AGGIORNAMENTO")
    private Date dataAggiornamento;

}
