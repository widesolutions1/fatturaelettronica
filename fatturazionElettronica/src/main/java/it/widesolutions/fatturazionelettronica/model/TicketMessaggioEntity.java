package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.TicketMessaggioPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="TICKET_MESSAGGIO")
@IdClass(TicketMessaggioPKs.class)
public class TicketMessaggioEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTICKET")
    private Integer idTicket;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDTICKET_MESSAGGIO")
    private Integer idTicketMessaggio;

    @Column(name="MESSAGGIO", columnDefinition = "TEXT")
    private String descrizione;

    @Column(name="DATA_INSERIMENTO")
    private Date titolo;

    @Column(name="ALLEGATO", columnDefinition = "TEXT")
    private String allegato;

    @Column(name="NOMEFILE")
    private String nomeFile;

    @Column(name="CSFILE")
    private String csFile;

    @Column(name="AGGDOC")
    private Date aggDoc;

    @Column(name="MIMETYPE")
    private String mimeType;

    @Column(name="MITTENTE")
    private String mittente;

    @Column(name="DESTINATARIO")
    private String destinatario;

}
