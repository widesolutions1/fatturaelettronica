package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.ImportPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="IMPORT")
@IdClass(ImportPKs.class)
public class ImportEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDIMPORTSESSION")
    private Integer idImportSession;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDIMPORT")
    private Integer idImport;

    @Column(name="APPLICATION_ID")
    private Integer applicationId;

    @Column(name="NAME")
    private String name;

    @Column(name="FILENAME")
    private String fileName;

    @Column(name="MIME_TYPE")
    private String mimeType;

    @Column(name="CREATED_ON")
    private Date createdOn;

    @Column(name="BLOB_CONTENT")
    private String blobContent;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @Column(name="XMLTYPEFATTURA", columnDefinition = "TEXT")
    private String xmlTypeFattura;

    @Column(name="RC")
    private Integer rc;

    @Column(name="ERRMSG")
    private String errMsg;

    @Column(name="CP_DENOMINAZIONE")
    private String cpDenominazione;

    @Column(name="CC_DENOMINAZIONE")
    private String ccDenominazione;

    @Column(name="CP_IDCODICE")
    private String cpIdCodice;

    @Column(name="CP_IDPAESE")
    private String cpIdPaese;

    @Column(name="CC_IDCODICE")
    private String ccIdCodice;

    @Column(name="CC_IDPAESE")
    private String ccIdPaese;

    @Column(name="DIREZIONE")
    private String direzione;

    @Column(name="RC_ESITO")
    private Integer rcEsito;

    @Column(name="ERRMSG_ESITO")
    private String errMsgEsito;

}
