package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.AllegatiPKs;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="ALLEGATI")
@IdClass(AllegatiPKs.class)
public class AllegatiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDALLEGATO")
    private Integer idAllegato;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="NOMEATTACHMENT")
    private String nomeAttachment;

    @Column(name="ALGORITMOCOMPRESSIONE")
    private String algoritmoCompressione;

    @Column(name="FORMATOATTACHMENT")
    private String formatoAttachment;

    @Column(name="DESCRIZIONEATTACHMENT")
    private String descrizioneAttachment;

    @Column(name="ATTACHMENT", columnDefinition = "TEXT") //blob! in prova
    private String attachment;

    @Column(name="MIME_TYPE")
    private String mimeType;

    @Column(name="AGGDOC")
    private Date aggDoc;

    @Column(name="CSFILE")
    private String csFile;

    @Column(name="NUMEROALLEGATO")
    private Integer numeroAllegato;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})
    public FatturaElettronicaEntity fatturaElettronicaToAllegati;

}
