package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="MODALITAINVIO")
public class ModalitaInvioEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDMODALITAINVIO")
    private String idModalitaInvio;

    @Column(name="MODALITAINVIO")
    private String modalitaInvio;

}
