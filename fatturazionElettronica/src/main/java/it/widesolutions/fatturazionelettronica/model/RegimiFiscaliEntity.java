package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="REGIMIFISCALI")
public class RegimiFiscaliEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="CODICE_REGIME_FISCALE")
    private String codiceRegimeFiscale;

    @Column(name="REGIME_FISCALE")
    private String regimeFiscale;

}
