package it.widesolutions.fatturazionelettronica.model;

import it.widesolutions.fatturazionelettronica.keys.DatiPagamentoPKs;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name="DATIPAGAMENTO")
@IdClass(DatiPagamentoPKs.class)
public class DatiPagamentoEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDDATIPAGAMENTO")
    private Integer idDatiPagamento;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDFATTURAELETTRONICA")
    private Integer idFatturaElettronica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDUTENTE")
    private Integer idUtente;

    @Column(name="CONDIZIONIPAGAMENTO")
    private String condizioniPagamento;

    @Column(name="DATAINSERIMENTO")
    private Date dataInserimento;

    @Column(name="DATAAGGIORNAMENTO")
    private Date dataAggiornamento;

    @ManyToOne
    @JoinColumns({@JoinColumn(name="IDFATTURAELETTRONICA", insertable = false, updatable = false),
            @JoinColumn(name="IDUTENTE", insertable = false, updatable = false)})

    public FatturaElettronicaEntity fatturaElettronicaToDatiPagamento;

}
