package it.widesolutions.fatturazionelettronica.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="NATURACESSIONI")
public class NaturaCessioniEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IDNATURACESSIONE")
    private String idNaturaCessione;

    @Column(name="NATURACESSIONE")
    private String naturaCessione;

    @Column(name="SELEZIONABILE")
    private String selezionabile;

}
