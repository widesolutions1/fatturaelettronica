package it.widesolutions.fatturazionelettronica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;

@SpringBootApplication
public class FatturazionelettronicaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FatturazionelettronicaApplication.class, args);
    }

}
